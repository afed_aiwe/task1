package com.task.angelina.listofcontacts.data;

import java.util.List;

public class User {

    private Integer id;
    private String firstName;
    private String secondName;
    private String avatar;
    private List<String> contact;

    public User() {
    }

    public User(String firstName, String secondName, String avatar) {
        this.firstName = firstName;
        this.secondName = secondName;
        this.avatar = avatar;
    }

    public User(Integer id, String firstName, String secondName, String avatar,
                List<String> contact) {
        this.id = id;
        this.firstName = firstName;
        this.secondName = secondName;
        this.avatar = avatar;
        this.contact = contact;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public List<String> getContact() {
        return contact;
    }

    public void setContact(List<String> contact) {
        this.contact = contact;
    }
}
