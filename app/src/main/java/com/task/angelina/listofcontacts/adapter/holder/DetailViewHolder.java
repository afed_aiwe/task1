package com.task.angelina.listofcontacts.adapter.holder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.task.angelina.listofcontacts.R;

public class DetailViewHolder extends RecyclerView.ViewHolder {

    public ImageView imgIcon;
    public TextView txtCharacter;
    public TextView txtGroup;
    public View divider;

    public DetailViewHolder(@NonNull View itemView) {
        super(itemView);

        imgIcon = itemView.findViewById(R.id.ic_phone);
        txtCharacter = itemView.findViewById(R.id.txt_character);
        txtGroup = itemView.findViewById(R.id.txt_group);
        divider = itemView.findViewById(R.id.divider);
    }

}
