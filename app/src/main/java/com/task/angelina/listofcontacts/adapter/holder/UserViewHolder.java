package com.task.angelina.listofcontacts.adapter.holder;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.task.angelina.listofcontacts.R;
import com.task.angelina.listofcontacts.data.User;

public class UserViewHolder extends RecyclerView.ViewHolder {

    private Context context;
    private ImageView imgAvatar;
    private TextView txtFullName;
    private TextView txtHeader;

    public UserViewHolder(@NonNull View itemView, Context context) {
        super(itemView);
        imgAvatar = itemView.findViewById(R.id.img_avatar);
        txtFullName = itemView.findViewById(R.id.txt_full_name);
        txtHeader = itemView.findViewById(R.id.txt_header);
        this.context = context;
    }

    public void bindListView(User user) {
        Glide.with(context)
                .load(context.getResources().getIdentifier(user.getAvatar(),"drawable", context.getPackageName()))
                .apply(RequestOptions.circleCropTransform())
                .into(imgAvatar);
        txtFullName.setText(user.getFirstName()+" "+user.getSecondName());
    }

    public void bindHeaderView(String string) {
        txtHeader.setText(string);
    }
}
