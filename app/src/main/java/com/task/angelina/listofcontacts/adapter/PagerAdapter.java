package com.task.angelina.listofcontacts.adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import com.task.angelina.listofcontacts.fragment.ContactFragment;

public class PagerAdapter extends FragmentStatePagerAdapter {

    private int mNumTabs;

    public PagerAdapter(FragmentManager fm, int numTabs) {
        super(fm);
        mNumTabs = numTabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return ContactFragment.newInstance(position);
            case 1:
                return ContactFragment.newInstance(position);
            case 2:
                return ContactFragment.newInstance(position);
                default:
                    return ContactFragment.newInstance(position);
        }
    }

    @Override
    public int getCount() {
        return mNumTabs;
    }
}
