package com.task.angelina.listofcontacts.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.task.angelina.listofcontacts.R;
import com.task.angelina.listofcontacts.adapter.holder.DetailViewHolder;
import com.task.angelina.listofcontacts.data.Conditional;
import com.task.angelina.listofcontacts.data.User;

public class DetailViewAdapter extends RecyclerView.Adapter<DetailViewHolder> {


    private User user;

    public DetailViewAdapter(User user) {
        this.user = user;
    }

    @NonNull
    @Override
    public DetailViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.contact_info, parent, false);
        return new DetailViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull DetailViewHolder holder, int position) {
        final String character = user.getContact().get(position);
        String chars = character.substring(0,1);

        holder.txtCharacter.setText(character);
        if (chars.equals("+")) {
            holder.txtGroup.setText(Conditional.MOBILE);
        } else if (chars.substring(0,1).equals("(")) {
            holder.txtGroup.setText(Conditional.WORK);
            if (getItemCount() > 1) {
                holder.imgIcon.setVisibility(View.GONE);
            }
        } else {
            holder.txtGroup.setText(Conditional.HOME);
            holder.imgIcon.setImageResource(R.drawable.ic_email_grey_24dp);
            if (getItemCount() > 1) {
                holder.divider.setVisibility(View.VISIBLE);
            }
        }

    }

    @Override
    public int getItemCount() {

        return user.getContact().size();
    }
}
