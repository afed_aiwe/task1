package com.task.angelina.listofcontacts.data;

import java.util.ArrayList;
import java.util.List;

public final class Conditional {
    public static final String WORK = "Work";
    public static final String MOBILE = "Mobile";
    public static final String HOME = "Home";

    private Conditional() {}

    public static List<User> getUserList() {
        List<User> userArrayList = new ArrayList<>();

        userArrayList.add(new User(1, "Peter", "Avis", "peteravis", new ArrayList<String>(){{
            add("+7 333 782-42-32"); add("(812) 782-42-32"); add("peter@mail.com");}}));

        userArrayList.add(new User(2, "Oscar", "Armer", "oscararmer",new ArrayList<String>(){{
            add("+7 333 782-42-32"); add("(812) 782-42-32"); add("oscar@mail.com");}}));

        userArrayList.add(new User(3, "Angel", "Dillon", "angeldillon", new ArrayList<String>(){{
            add("+7 333 782-42-32"); add("(812) 782-42-32");}}));

        userArrayList.add(new User(4, "Arthur", "Dykes", "arthurdykes", new ArrayList<String>(){{
            add("(812) 782-42-32"); add("arthur@mail.com");}}));

        userArrayList.add(new User(5, "Rosie", "Downey", "rosiedowney", new ArrayList<String>(){{
            add("+7 333 782-42-32"); add("rosie@mail.com");}}));

        userArrayList.add(new User(6, "Roy", "Furse", "royfurse",  new ArrayList<String>(){{
            add("+7 333 782-42-32"); add("roy@mail.com");}}));

        userArrayList.add(new User(7, "Oscar", "Farrier", "oscarfarrier", new ArrayList<String>(){{
            add("+7 333 782-42-32");}}));
        return userArrayList;
    }
}
