package com.task.angelina.listofcontacts.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.task.angelina.listofcontacts.R;
import com.task.angelina.listofcontacts.adapter.holder.UserViewHolder;
import com.task.angelina.listofcontacts.data.User;
import java.util.Collections;
import java.util.List;

public class UserViewAdapter extends RecyclerView.Adapter<UserViewHolder> {

    private List<User> list;
    private User oldUser;

    public UserViewAdapter(List<User> list) {
        this.list = list;
        Collections.sort(this.list, (user, t1) -> user.getSecondName().compareTo(t1.getSecondName()));
    }

    @NonNull
    @Override
    public UserViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final UserViewHolder viewHolder;
        Context context = parent.getContext();
        View userView = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_list, parent, false);
        viewHolder = new UserViewHolder(userView, context);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull UserViewHolder holder, int position) {
        final User user = list.get(position);
        holder.bindListView(user);
        if (position > 0) {
            oldUser = list.get(position - 1);
            String s = oldUser.getSecondName().substring(0, 1);
            if (!s.equalsIgnoreCase(user.getSecondName().substring(0,1))) {
                holder.bindHeaderView(user.getSecondName().substring(0, 1));
            }
        } else {
            holder.bindHeaderView(user.getSecondName().substring(0,1));
        }
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

}

